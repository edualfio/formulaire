exports.capitalizeFirstLetter = function(string) {
    return string.replace(/^./, string[0].toUpperCase());
}