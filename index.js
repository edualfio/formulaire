var _ = require('lodash');
var functions = require('./lib/functions.js')


var tipology = ['text','password','color','date','datetime-local','email','month','number','range','search','tel','time','url','week']


module.exports = function(params) {

	var t = ''; // template

	// Almacenaremos en este array todos los campos ya pintados
	var fT = [] // fieldTemplate

	// Mapping fields and convert it into html code
	_.map(params.fields, function(v,k){
		fT.push(extract(v, k, params.fieldClassWrapper, params.fieldClass))
	})


	t = '<form id="' + params.formId + '" class="' + params.formClass + '" action="' + params.action + '" method="' + params.method + '">';
	_.map(fT, function(v,k){
		t += v
	});


	t += '<div class="' + params.fieldClassWrapper + '">';
	if( params['submit'].type === "input") {
		t += '<input class="' + params['submit'].clase + '" type="submit" value="' + params['submit'].value + '">';
	} else if(params['submit'].type === "link") {
		t += '<a class="' + params['submit'].clase + '" href="' + params['submit'].href + '" value="' + params['submit'].value + '">' + params['submit'].value + '</a>';
	} else if(params['submit'].type === "button") {
		t += '<button class="' + params['submit'].clase + '" type="button" onclick="' + params['submit'].onclick + '"';
	} 
	t += '</div>';

	t += '</form>';

	return t;

};



function extract(field, name, wrap, clase){

	var pF = ''; //Processed Field

	if ( _.includes(tipology, field.type) ) {

		if (field.required) {var required = 'required'} else {var required = ''}

		pF += '<div class="' + wrap + '">';
		pF += '<label for="'+ name +'">'+ functions.capitalizeFirstLetter(name) + '</label>';
		pF += '<input type="' + field.type + '" name="' + name + '" id="' + name + '" class="' + clase + '" '+ required +'>'
		pF += '</div>'

	} else if (field.type === 'radio' ||  field.type === 'checkbox') {

		pF += '<div class="' + wrap + '">';
		pF += '<label>'+ functions.capitalizeFirstLetter(name) + '</label>';
		_.map(field.options, function(v,k){

			if (v.checked) {var checked = 'checked'} else {var checked = ''}
			if (field.required) {var required = 'required'} else {var required = ''}

			pF += '<div class="'+ field.type +'">';
			pF += '<label for="' + name + '_' + k + '">';
			pF += '<input type="'+ field.type +'" name="' + name + '" id="' + name + '_' + k + '" value="' + k + '" '+ checked + required +'>';
			pF += functions.capitalizeFirstLetter(v.label)
			pF += '</label>';
			pF += '</div>'
		});
		pF += '</div>'


	}  else if (field.type === 'select') {

		pF += '<div class="' + wrap + '">';
		pF += '<label>'+ functions.capitalizeFirstLetter(name) + '</label>';
		pF += '<select class="form-control" name="' + name + '" id="' + name + '">';
		if (field.default){
			pF += '<option value="">' + field.default + '</option>';
		}
		_.map(field.options, function(v,k){
			if (v.selected) {var selected = 'selected'} else {var selected = ''}
			pF += '<option value="' + k + '" '+ selected + '>' + v.label + '</option>';
		});
		pF += '</select>'
		pF += '</div>'

	} else if (field.type === 'textarea') {

		pF += '<div class="' + wrap + '">';
		pF += '<label>'+ functions.capitalizeFirstLetter(name) + '</label>';
		pF += '<textarea name="' + name + '" id="' + name + '" class="' + clase + '" '+ required +'></textarea>'
		pF += '</div>'

	} else {
		console.log('Field\'s Category not match');
		pF += '<div class="' + wrap + '">';
		pF += '<strong> >>>> Field Error (' + name + ')</strong>'
		pF += '</div>'
	}

	return pF;
}