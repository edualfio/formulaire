# README #

Developing

### Install ###


```
#!bash

npm install formulaire
```

(formulaire uses lodash)


### Using formulaire ###


```
#!javascript

var formulaire = require('formulaire');

var myForm = formulaire({
    //options
});

```

Returns cool html code ready to use


### API Options ###

No default values (pending to do)


<table>
	<thead>
		<tr>
			<th>Option</th>
			<th>Value</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
    	<tr>
    		<td>formClass</td>
    		<td>STRING</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td>formId</td>
    		<td>STRING</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td>action</td>
    		<td>STRING</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td>method</td>
    		<td>POST | GET</td>
    		<td></td>
    	</tr>
    	<tr>
    		<td>fieldClassWrapper</td>
    		<td>STRING</td>
    		<td>Equivalent class to wrapper of the field</td>
    	</tr>
    	<tr>
    		<td>fieldClass</td>
    		<td>STRING</td>
    		<td>Class definition to inputs</td>
    	</tr>
    	<tr>
    		<td>submit.type</td>
    		<td>link | button | input</td>
    	</tr>
    	<tr>
    		<td>submit.clase</td>
    		<td>STRING</td>
    	</tr>
    	<tr>
    		<td>submit.href</td>
    		<td>STRING</td>
    	</tr>
    	<tr>
    		<td>submit.onclick</td>
    		<td>STRING</td>
    	</tr>
    	<tr>
    		<td>submit.field</td>
    		<td>STRING</td>
    	</tr>
    	<tr>
    		<td>field.type</td>
    		<td>text | password | color | date | datetime-local | email | month | number | range | search | tel | time | url | week | radio | checkbox | select | textarea </td>
    	</tr>
    </tbody>
</table>

### Code Example ###

```
#!javascript

var formulario = formulaire({
		formClass : 'form',
	    formId : 'formulario_ID',
		action: '/send',
	    method: 'post',
	    fieldClassWrapper: 'form-group',
	    fieldClass: 'form-control',
	    submit: { 
	        type: 'link',
	        clase: 'btn btn-primary sender',
	        href: 'javascript:void(0)',
	        onclick: null,
	        value: 'Enviar'
	    },
		fields: {
			name: { type: 'text', required: true },
			surname: { type: 'text', required: true},
			email: { type: 'email', required: true},
			age: { type: 'number'},
			country: {
				type: 'select',
				default: 'Select your country',
				required: true,
				options: {
					spain: { label: 'Spain', selected: true},
					usa: { label: 'USA'}
				}
			},
			sex: {
				type: 'radio',
				options: {
					m: { label: 'Male'},
					f: { label: 'Female'}
				}
			},
			preferences: {
				type: 'checkbox',
				options: {
					soccer: { label: 'Soccer', checked: true },
					basket: { label: 'Basket'},
					tennis: { label: 'Tennis'},
					volley: { label: 'Volley'},
					hockey: { label: 'Hockey'},
				}
			},
			observations: { type: 'textarea', required: true }
		}
	});

```

Now, you can use an JSON file to define the form

```
#!javascript
var defForm = require('./json/form_example.json');
var formulario = formulaire(defForm);

```